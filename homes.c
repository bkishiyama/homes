#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "homes.h"

// TODO: Write a function called readhomes that takes the filename
// of the homes file and returns an array of pointers to home structs
// terminated with a NULL
struct home ** readhomes(char * filename)
{  
    FILE * f = fopen(filename,"r");
    //inital size of array
    int size = 100;
    int addr_size = 50;
    //create the homes array
    struct home ** homes;
    homes = (struct home **)malloc(size * sizeof(struct home *));
    //account for the amount of memory required to store the home struct and the addr within the home struct
    for( int i = 0; i < size; i++)
    {   
        homes[i] =(struct home *)malloc(sizeof(struct home));
        homes[i]->addr = (char *)malloc(addr_size*sizeof(char));
    }
    //variables used in the scan 
    int count = 0;
    int newsize = size;   
    int zip;
    char addr[50];
    int price;
    int area;
    //scan the file
    while( fscanf(f,"%d,%[^,],%d,%d\n", &zip, addr, &price, &area) != EOF)
    {
        homes[count]->zip = zip;
        strcpy(homes[count]->addr, addr);
        homes[count]->price = price;
        homes[count]->area = area;
        //reallocate space if required
        if( count == (size - 1))//account for Null
        {
            size += 100;
            struct home ** newarray = (struct home **)realloc(homes,size * sizeof(struct home *));
            homes = newarray;
            //account for the home struct and the addr of the home struct
            while( newsize < size)
            {
                homes[newsize] = (struct home *)malloc(sizeof(struct home));
                homes[newsize]->addr = (char *)malloc(addr_size*sizeof(char));
                newsize++;
            }
       }
       count++; 
    }   
    homes[count] = NULL;
    fclose(f);
    return homes;
}
//Calculate the total number of homes
int numhomes(struct home **homesarray)
{
    int count = 0;
    while (homesarray[count] != NULL)
    {
    count++;
    }
    
    return count;
}

//check how many homes have a specific zipcode
int ziphomes(int zip, int numhomes, struct home **homesarray)
{   
    int count = 0;

    for(int i = 0; i<numhomes; i++)
    {
        if(homesarray[i]->zip == zip)
        {
            count++;
        } 
    }
    return count;
}

//function to check how many homes are within a range of 10000 dollars to the price entered. It will also print out the address of the homes within range as well as the price.
void pricehomes(int hprice, int numhomes, struct home **homesarray)
{
    for (int i = 0; i<numhomes; i++)
    {
        if(homesarray[i]->price <= hprice+10000 && homesarray[i]->price >= hprice-10000)
        {
            printf("Price of %s: %d$ \n", homesarray[i]->addr, homesarray[i]->price); 
        }
    }
}


//Main
int main(int argc, char *argv[])
{
    // TODO: Use readhomes to get the array of structs
    struct home **homes = readhomes(argv[1]);
    // At this point, you should have the data structure built
    // TODO: How many homes are there? Write a function, call it,
    // and print the answer.
    int total = numhomes(homes);
    printf("Total is %i\n", total);
    // TODO: Prompt the user to type in a zip code.
    int zipcode = 0;
    printf("Enter a zipcode: ");
    scanf("%d", &zipcode);
    // At this point, the user's zip code should be in a variable
    // TODO: How many homes are in that zip code? Write a function,
    // call it, and print the answer.
    int totalzip = ziphomes(zipcode, total, homes);
    printf("Total homes with this zipcode is: %d\n", totalzip);
    // TODO: Prompt the user to type in a price.
    int price = 0;
    printf("Enter a price: ");
    scanf("%d", &price);    
    // At this point, the user's price should be in a variable
    // TODO: Write a function to print the addresses and prices of 
    // homes that are within $10000 of that price.
    pricehomes(price, total, homes);
}
